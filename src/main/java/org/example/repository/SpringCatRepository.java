package org.example.repository;

import org.example.model.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SpringCatRepository implements CatRepository{

   @Autowired   private DataSource dataSource;
   @Autowired private RowMapper<Cat> rowMapper;
    private JdbcTemplate jdbcTemplate;

    public void init(){
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.execute("CREATE TABLE cats (Name VARCHAR(45),Id LONG, Weight INT, IsAngry BIT)");
       /* create(new Cat("Murzik", 1L, 8, true));
        create(new Cat("Barsik", 2L, 6, false));
        create(new Cat("Murka", 3L, 2, false));
        create(new Cat("Borya", 4L, 6, true));*/
    }

    @Override
    public List<Cat> findAll() {
        return new ArrayList<>(jdbcTemplate.query("SELECT * FROM cats", rowMapper));
    }

    @Override
    public boolean create(Cat element) {
        return jdbcTemplate.update("INSERT INTO cats(id, Name, Weight, isAngry) VALUES (?, ?, ?, ?)",
                element.getId(),
                element.getName(),
                element.getWeight(),
                element.isAngry()
                )>0;
    }

    @Override
    public Cat read(Long id) {
        return jdbcTemplate.query("SELECT * FROM cats WHERE id=?", new BeanPropertyRowMapper<>(Cat.class), id).get(0);
    }

    @Override
    public int update(Long id, Cat element) {
        return jdbcTemplate.update("UPDATE cats SET Id=?, Name=?, Weight=?, isAngry=? WHERE Id = ?",
                element.getId(),
                element.getName(),
                element.getWeight(),
                element.isAngry(),
                id);
    }

    @Override
    public boolean delete(Long id) {
        return jdbcTemplate.update("DELETE cats WHERE id = ?",id)>0;
    }
}
