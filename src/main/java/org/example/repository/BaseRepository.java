package org.example.repository;

import java.util.List;

public interface BaseRepository <E, K> {

    List<E> findAll();
      boolean create(E element);
      E read(K id);
      int update(K id, E element);
      boolean delete(K id);



}
