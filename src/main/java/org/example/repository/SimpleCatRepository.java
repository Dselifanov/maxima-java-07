package org.example.repository;

import org.example.model.Cat;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleCatRepository implements CatRepository {


    private final String table = "Cats";
    public static final String DB_DRIVER = "org.h2.Driver";
    public static final String DB_URL = "jdbc:h2:mem:test";
    // public static final String TABLE = "CREATE TABLE Cats (Name VARCHAR(45), Weight INT)";

    /*public SimpleCatRepository( String table) {

        this.table = table;
    }*/
    public void advancedCatRepository() {
       try {
           Class.forName(DB_DRIVER);
           Connection connection = DriverManager.getConnection(DB_URL);
           System.out.println("Соединение с БД выполнено");
           Statement statement = connection.createStatement();
           int row1 = statement.executeUpdate("CREATE TABLE cats (Id LONG, Name VARCHAR(45), Weight INT, IsAngry BIT )");
           System.out.println("Создано таблиц " + row1 + " c наименованием " + table);
       }
         catch( ClassNotFoundException e)        {            e.printStackTrace();            System.out.println("Нет драйвера!!!");

        }
        catch( SQLException e)        {
            e.printStackTrace();
            System.out.println("Ошибка SQL!!!");

        }
    }



    @Override
    public boolean create(Cat element) {

            try{
                Class.forName(DB_DRIVER);
                Connection connection = DriverManager.getConnection(DB_URL);
                System.out.println("Соединение с БД выполнено");
                Statement statement = connection.createStatement();
               // int row2 = statement.executeUpdate("INSERT INTO cats (name, weight) VALUES ('Murzik', 10)");
                String sql1 = "INSERT INTO cats (id, name, weight, isAngry) VALUES (%d, '%s', %d, '%b')";
                int row2 = statement.executeUpdate(String.format(sql1,
                        element.getId(), element.getName(), element.getWeight(), element.isAngry()));
            connection.close();
                System.out.println("Добавлено элементов " + row2);
            System.out.println("Соединение с БД отключено");
            return row2>0;
            }

        catch (ClassNotFoundException e){
            e.printStackTrace();
            System.out.println("Нет драйвера!!!");
            return false;
        }
        catch (SQLException e){
            e.printStackTrace();
            System.out.println("Ошибка SQL!!!");
            return false;
        }

    }

    @Override
    public Cat read(Long id) {
        try {
            Class.forName(DB_DRIVER);

            Connection connection = DriverManager.getConnection(DB_URL);
            System.out.println("Соединение с БД выполнено");
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(String.format("SELECT * FROM cats WHERE id = '%d'", id));
            resultSet.next();
            Cat cat = new Cat( resultSet.getLong("Id"),resultSet.getString("Name"),
                                resultSet.getInt("Weight"), resultSet.getBoolean("IsAngry"));
            System.out.println(cat);
            connection.close();
            System.out.println("Соединение с БД отключено");
            return cat;
        }
        catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ошибка SQL!!!");
            return null;
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Нет драйвера!!!");
            return null;
        }
    }

    @Override
    public int update(Long id, Cat element) {
        try {
            Class.forName(DB_DRIVER);

            Connection connection = DriverManager.getConnection(DB_URL);
            System.out.println("Соединение с БД выполнено");
            Statement statement = connection.createStatement();
            String sql2 = "UPDATE cats SET Id= %d, Name = '%s', Weight = %d, IsAngry = '%b' WHERE Id= %d";
            int row3 = statement.executeUpdate(String.format(sql2, element.getId(), element.getName(), element.getWeight(), element.isAngry(), id));
            connection.close();
            System.out.println("Изменено котов " + row3);
            System.out.println("Соединение с БД отключено");
            return row3;

        }
        catch (SQLException e) {
             e.printStackTrace();
            System.out.println("Ошибка SQL!!!");
            return 0;
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Нет драйвера!!!");
            return 0;
        }
    }

    @Override
    public boolean delete(Long id) {
        try {
            Class.forName(DB_DRIVER);

            Connection connection = DriverManager.getConnection(DB_URL);
            System.out.println("Соединение с БД выполнено");
            Statement statement = connection.createStatement();
            String sql3 ="DELETE FROM cats WHERE Id = %d";
            int row = (statement.executeUpdate(String.format(sql3, id)));
            connection.close();
            System.out.println("Удалено котов " + row);
            System.out.println("Соединение с БД отключено");
        return row>0;
        }
        catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Ошибка SQL!!!");
                return false;
            }
        catch (ClassNotFoundException e) {
                e.printStackTrace();
                System.out.println("Нет драйвера!!!");
                return false;
            }
    }
    @Override
    public List<Cat> findAll() {

        try {
            Class.forName(DB_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL);
            System.out.println("Соединеение с БД выполнено");
            Statement statement = connection.createStatement();
            List<Cat> cats= new ArrayList<>();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM cats");
            while (resultSet.next()) {
                System.out.println(resultSet.getString("name"));
            }
            connection.close();
            System.out.println("Соединеение с БД отключено");
        return cats;
        }
        catch (ClassNotFoundException e){
                e.printStackTrace();
                System.out.println("Нет драйвера!!!");
                return null;
            }
        catch (SQLException e){
                e.printStackTrace();
                System.out.println("Ошибка SQL!!!");
                return null;
            }
    }
}
