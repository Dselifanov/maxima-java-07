package org.example.repository;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.example.model.Cat;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.function.Function;

public class AdvancedCatRepository implements CatRepository{



    private HikariConfig config = new HikariConfig();
    private HikariDataSource dataSource;

    private static Connection connection;



public void advancedCatRepository()  {

    Properties dbProps = new Properties();
    String propertiesPath = "src/database.properties";
    System.out.println("Соединение с БД");
    try {
        dbProps.load(new FileInputStream(propertiesPath));

        config.setJdbcUrl(dbProps.getProperty("db.url"));
        config.setDriverClassName(dbProps.getProperty("db.driver"));
        dataSource = new HikariDataSource(config);
        Connection connection = dataSource.getConnection();

        Statement statement = connection.createStatement();
        int row1 = statement.executeUpdate("CREATE TABLE cats (Id LONG, Name VARCHAR(45), Weight INT, IsAngry BIT)");


        System.out.println("Создано таблиц " + row1 + " c наименованием ");
    }
    catch (SQLException | IOException e){
        e.printStackTrace();
    }
}


    @Override
    public boolean create(Cat element) {

        String query = "INSERT INTO cats(id, Name, Weight, isAngry) VALUES (?, ?, ?, ?)";
        try {

            connection = dataSource.getConnection();
            System.out.println("Создаем");
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setLong(1, element.getId());
            preparedStatement.setString(2, element.getName());
            preparedStatement.setInt(3, element.getWeight());
            preparedStatement.setBoolean(4, element.isAngry());

            int row2 = preparedStatement.executeUpdate();
            System.out.println("Добавлено элементов " + row2);

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ошибка SQL!!!");
            return false;
        }
    }

    @Override
    public Cat read(Long id) {
        String query = "SELECT * FROM cats WHERE id =?";
        try {

            System.out.println("Читаем");
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                System.out.println(resultSet.getString("name"));
            }

        return null;

    } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public int update(Long id, Cat element) {
        String query = "UPDATE cats SET Id = ?, Name = ?, Weight = ?, IsAngry = ? WHERE Id = ?";
        try {
            System.out.println("Меняем");

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, element.getId());
            preparedStatement.setString(2, element.getName());
            preparedStatement.setInt(3, element.getWeight());
            preparedStatement.setBoolean(4, element.isAngry());
            preparedStatement.setLong(5, id);
            int rows = preparedStatement.executeUpdate();
            System.out.println(rows);
            return rows;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public boolean delete(Long id) {
        String query = "DELETE FROM cats WHERE Id = ?";
        try {
            System.out.println("Удаляем");
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            int rows =preparedStatement.executeUpdate();
            System.out.println("Удалено котов "+rows);
            return rows>0;
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Cat> findAll() {
        List<Cat> listOfCAts= new ArrayList<>();
        try {
            System.out.println("Читаем всех");

            String query = "SELECT * FROM cats";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet =preparedStatement.executeQuery();

           while (resultSet.next()){
               System.out.println(resultSet.getString("name"));
           }

            connection.close();
            return listOfCAts;

        }
        catch ( SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
