package org.example.model;

public class Cat {

    private long id;
    private String name;
    private int weight;
    private boolean isAngry;

    public Cat(long id, String name,  int weight, boolean isAngry) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.isAngry = isAngry;
    }

    protected Cat() {
    }

    @Override
    public String toString() {
        return "Cat{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                ", isAngry=" + isAngry +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isAngry() {
        return isAngry;
    }

    public void setAngry(boolean angry) {
        isAngry = angry;
    }
}
