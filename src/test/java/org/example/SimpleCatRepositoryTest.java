package org.example;

import org.example.model.Cat;

import org.example.repository.SimpleCatRepository;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

public class SimpleCatRepositoryTest {

    @Test
    public void catRepositoryCorrect() throws SQLException {
        SimpleCatRepository repo = new SimpleCatRepository();
        repo.advancedCatRepository();

        Cat murzik= new Cat(1,"Murzik",  5, true);
        Cat barsik= new Cat(2,"Barsik",  8 , true);
        Cat murka= new Cat(3,"Murka",  2, false);
        Cat borya = new Cat(4,"Borya",  12, true);

       // repo.advancedCatRepository();

        repo.create(murzik);
        repo.create(murka);
        repo.read(1L);
        repo.delete(2L);

        repo.update(1L, borya);
        repo.read(4L);

       List<Cat> cats= repo.findAll();
    }
}
