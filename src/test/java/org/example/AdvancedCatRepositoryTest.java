package org.example;

import org.example.model.Cat;
import org.example.repository.AdvancedCatRepository;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


public class AdvancedCatRepositoryTest {

    @Test
    public void catRepositoryCorrect()  {
        AdvancedCatRepository repo = new AdvancedCatRepository();

        Cat murzik= new Cat(1, "Murzik",  5, true);
        Cat barsik= new Cat(2, "Barsik",  8 , true);
        Cat murka= new Cat(3, "Murka",  2, false);

        repo.advancedCatRepository();

        repo.create(murzik);
        repo.create(barsik);
        repo.read(2L);
        repo.update(1L, murka);

        repo.read(1L);

        repo.delete(2L);

       repo.findAll().forEach(System.out::println);

    }

}
