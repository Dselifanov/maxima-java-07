package org.example;

import org.example.config.SpringConfig;
import org.example.model.Cat;
import org.example.repository.SimpleCatRepository;
import org.example.repository.SpringCatRepository;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.sql.SQLException;

public class SpringCatRepositoryTest {

    @Test
    public void catRepositoryCorrect() throws SQLException {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        SpringCatRepository repo = context.getBean(SpringCatRepository.class);
        repo.init();
        repo.create(new Cat(1L,"Murzik",  8, true));
        repo.create(new Cat(2L,"Barsik",  6, false));
        repo.create(new Cat(3L,"Murka",  2, false));
        repo.create(new Cat(4L,"Borya",  6, true));

        repo.update(1L, new Cat(5L,"Jorik", 18, true));
        repo.delete(3L);

        repo.findAll().forEach(System.out::println);

    }
}
